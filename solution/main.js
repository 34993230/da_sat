// AI Infographic for Data Analytics SAT
function init() {
	splashscreen();
	generateChart("demo", "bar");
	generateChart("demo1", "pie");
}; init();


// Handles splashscreen
function splashscreen() {
	var debug = document.getElementById("debug");
 	var target = document.getElementsByTagName("body")[0];
	var container = document.createElement("div");
	container.id = "splashscreen";
	var text_header = document.createElement("h1");
	text_header.id = "splashscreen_text"
	container.appendChild(text_header);
	target.appendChild(container);
	text_header.innerText = "Loading";
	var appended_splashscreen = document.getElementById("splashscreen");
	appended_splashscreen.style.display = "block";
	// animates loading text
	// need to clean up the code for timer (it should be possible to just move this into an Interval counter instead)
	setInterval(function() {
			var i = 0; 
		for (i < 3; i++) {
			text_header.innerText += ".";
			debug.innerText += i + "\n";
		};
			if (i == 3) {
				clearInterval
			}
		}, 1000);
	var timer = setTimeout(function() {

		appended_splashscreen.style.display = "none";

	}, 3000);
	
		// May want to add async support here, depending on the duration of the load times
}



// Handles chart generation
/*
TODOs with chart generation:
- create container object before generating chart
- dynamic formatting (could be achieved via the use of a table layout)
*/
function generateChart(name, type) {
var body_target = document.getElementById("body-content");
	var node_container = document.createElement("div");
	node_container.classList.add("jumbotron");
	node_container.id = "node_" + name;
	var canvas_element = document.createElement("canvas");
	canvas_element.id = name;
	node_container.appendChild(canvas_element);
	body_target.appendChild(node_container);
var ctx = document.getElementById(name).getContext('2d');
var myChart = new Chart(ctx, {
    type: type,
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
};