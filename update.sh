#****************************************************
# 	      SAT Updater Script by J.S
#	      Last Modified: 21/04/2020
#****************************************************

if [[ "$OSTYPE" == "linux-gnu" ]]; then
	echo "SAT Update Script created by J.S 2020"
	echo "[SAT Updater Script] Updating Repository..."
	git add .
	git commit -m "Updated: $(date)"
	echo "[SAT Updater Script] Pushing changes..."
	git push origin master
	echo "[SAT Updater Script] Done"
       
elif [[ "$OSTYPE" == "darwin"* ]]; then
  	echo "SAT Update Script created by J.S 2020"
	echo "[SAT Updater Script] Updating Repository..."
	git commit -m "Updated: $(date)"
	echo "[SAT Updater Script] Pushing changes..."
	git push origin master
	echo "[SAT Updater Script] Done"

elif [[ "$OSTYPE" == "win32" ]]; then
	echo "[SAT Updater] MS Windows is Currently unsupported"
else
	echo "[SAT Updater] Unknown Operating System"	

fi
